## Juxtapose Before After Slider

This module provides a new formatter for image field
with Before After sliding effect to compare images.

## INSTALLATION

- Download and enable this module.
- Download the **[JuxtaposeJS](https://github.com/NUKnightLab/juxtapose)**
  library and extract it to `/libraries` directory,
  correct path for the javascript file should be
  `/libraries/juxtapose/build/js/juxtapose.js`.<br>
  The **JuxtaposeJS** library can also be automatically installed
  using the `composer.libraries.json` file.
- Select **Juxtapose Before After Slider** formatter in your multivalued
  image field display configuration,
- Remember that you have to upload at least 2 images.
