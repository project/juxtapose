(function($, Drupal, once) {

  Drupal.behaviors.juxtapose = {
    attach: function(context) {
      once('juxtapose', '.juxtapose-container', context).forEach(function (element) {
          // Nothing to do.
      });
    }
  }

})(jQuery, Drupal, once);
