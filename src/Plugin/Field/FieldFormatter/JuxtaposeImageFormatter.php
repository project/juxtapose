<?php

namespace Drupal\juxtapose\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Juxtapose Slider' image formatter.
 *
 * @FieldFormatter(
 *   id = "juxtapose_image",
 *   label = @Translation("Juxtapose Before After Slider"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class JuxtaposeImageFormatter extends ImageFormatter implements ContainerFactoryPluginInterface {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs an JuxtaposeImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   The image style storage.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, EntityStorageInterface $image_style_storage, FileUrlGeneratorInterface $file_url_generator, EntityFieldManagerInterface $entity_field_manager, $logger_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $current_user, $image_style_storage, $file_url_generator);
    $this->entityFieldManager = $entity_field_manager;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('file_url_generator'),
      $container->get('entity_field.manager'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'image_style' => '',
      'starting_position' => '',
      'additional_options' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();

    $image_styles = image_style_options(FALSE);
    $description_link = Link::fromTextAndUrl(
      $this->t('Configure Image Styles'),
      Url::fromRoute('entity.image_style.collection')
    );
    $elements['image_style'] = [
      '#title' => $this->t('Image style'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
      '#description' => $description_link->toRenderable() + [
        '#access' => $this->currentUser->hasPermission('administer image styles'),
      ],
    ];

    $elements['starting_position'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Starting position'),
      '#description' => $this->t('The starting position is expressed as a percentage, ex: 70.  Defaults to 50 if empty.'),
      '#default_value' => $settings['starting_position'],
      '#placeholder' => 'Ex: 30',
    ];

    $elements['additional_options'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#title' => $this->t('Additional options'),
      '#description' => $this->t('Allows to define additional options for the Juxtapose image formatter, ex: show_labels=false to remove labels.'),
      '#default_value' => $settings['additional_options'],
      '#placeholder' => 'Ex: show_labels=false',
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary = [];

    if ($settings['image_style']) {
      $summary[] = $this->t('Image style: @image_style', ['@image_style' => $settings['image_style']]);
    }

    if ($settings['starting_position']) {
      $summary[] = $this->t('Starting position: @starting_position', ['@starting_position' => $settings['starting_position']]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $settings = $this->getSettings();

    $options = [];
    $options['starting_position'] = $settings['starting_position'] ?? '50%';

    $additional_options = self::parseKeyValueString($settings['additional_options']);
    $options = array_merge($options, $additional_options);

    try {
      /** @var \Drupal\image\ImageStyleInterface $style */
      $style = $this->imageStyleStorage->load($settings['image_style']);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('juxtapose')->warning($e->getMessage());
    }
    $images = [];

    foreach ($items as $item) {
      $url = isset($style) ? $style->buildUrl($item->entity->getFileUri()) : $this->fileUrlGenerator->generateAbsoluteString($item->entity->getFileUri());
      $alt = $item->get('alt')->getValue();
      $title = $item->get('title')->getValue();
      $image = [];
      $image['url'] = $url;
      $image['alt'] = $alt;
      $image['label'] = $title;
      $images[] = $image;
      if (count($images) == 2) {
        break;
      }
    }

    return [
      '#theme' => 'juxtapose_slider_image',
      '#images' => $images,
      '#options' => $options,
      '#attached' => [
        'library' => [
          'juxtapose/juxtapose',
        ],
      ],
    ];
  }

  /**
   * Extract "key=value" pairs from multiline text.
   *
   * Fonction qui analyse une chaîne de caractères multiligne où chaque ligne
   * contient un couple clé-valeur séparé par un signe égal, et qui renvoie un
   * tableau associatif de ces clés et valeurs.
   *
   * @param string $string
   *   La chaîne à analyser.
   *
   * @return array
   *   Le tableau associatif contenant les clés et les valeurs.
   */
  public static function parseKeyValueString(string $string): array {
    // Tableau pour stocker les résultats.
    $result = [];
    // Sépare la chaîne en lignes.
    $lines = explode("\n", $string);

    foreach ($lines as $line) {
      // Vérifie si la ligne n'est pas vide.
      if (trim($line) != '') {
        // Sépare la ligne en deux au niveau du signe égal.
        [$key, $value] = explode('=', $line, 2);
        // Nettoie les espaces blancs autour de la clé.
        $key = trim($key);
        // Nettoie les espaces blancs autour de la valeur.
        $value = trim($value);
        if ($key != '') {
          // Ajoute le couple clé-valeur au tableau résultat.
          $result[$key] = $value;
        }
      }
    }

    // Retourne le tableau associatif.
    return $result;
  }

}
